
'use strict';

import { combineReducers } from 'redux';

import drawer from './drawer';
import route from './route';
import users from './users';

export default combineReducers({

 	drawer,
 	route,
  users

})
