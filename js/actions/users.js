
'use strict';
import { checkHttpStatus, parseJSON } from '../utils';
import { AsyncStorage } from 'react-native';
import axios from 'axios';

import type {Action} from './types';

//Sign Up User
export const SIGNUP_USER = 'SIGNUP_USER';
export const SIGNUP_USER_SUCCESS = 'SIGNUP_USER_SUCCESS';
export const SIGNUP_USER_FAILURE = 'SIGNUP_USER_FAILURE';
export const RESET_USER = 'RESET_USER';

//Sign In User
export const SIGNIN_USER = 'SIGNIN_USER';
export const SIGNIN_USER_SUCCESS = 'SIGNIN_USER_SUCCESS';
export const SIGNIN_USER_FAILURE = 'SIGNIN_USER_FAILURE';

//log out user
export const LOGOUT_USER = 'LOGOUT_USER';

const ROOT_URL = 'http://laravel.localhost/api/admin';

export function signUpUser(formValues) {
  const request = axios.post(`${ROOT_URL}/auth/signup`, formValues);

  return {
    type: SIGNUP_USER,
    payload: request
  };
}

export function signUpUserSuccess(user) {
  return {
    type: SIGNUP_USER_SUCCESS,
    payload: user
  };
}

export function signUpUserFailure(error) {
  return {
    type: SIGNUP_USER_FAILURE,
    payload: error
  };
}


export function resetUser() {
  return {
    type: RESET_USER,
  };
}

export function signInUser() {
  // const request = axios.post(`${ROOT_URL}/auth/login`, formValues);

  return {
    type: SIGNIN_USER,
    isFetching: true
  };
}

export function signInUserSuccess(user) {
  return {
    type: SIGNIN_USER_SUCCESS,
    payload: user
  };
}

export function signInUserFailure(error) {
  return {
    type: SIGNIN_USER_FAILURE,
    payload: error
  };
}

export function logoutUser() {
  return {
    type: LOGOUT_USER
  };
}

export function signInUserAsync(values) {
  return dispatch => {

     dispatch(signInUser());
     var fdata = new FormData();
     fdata.append('email', values.email);
     fdata.append('password', values.password);
      return fetch(`${ROOT_URL}/auth/login`, {
         method: 'POST',
         headers: {
           'Accept': 'application/json',
           'Content-Type': 'application/json',
          },
         body: fdata
       })
       .then(checkHttpStatus)
       .then(parseJSON)
       .then(response => {
         console.log(response);
            try {
                // let decoded = jwtDecode(response.result.token);
                AsyncStorage.setItem('jwtToken', response.result.token);
                dispatch(signInUserSuccess(response.result));
            } catch (e) {
                dispatch(signInUserFailure({
                    response: {
                        status: 403,
                        statusText: 'Invalid token'
                    }
                }));
            }
        })
        .catch((err) => {
          dispatch(signInUserFailure(err));
        });

    };
}
