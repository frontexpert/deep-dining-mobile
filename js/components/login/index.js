
'use strict';

import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { DeviceEventEmitter, Dimensions, Image } from 'react-native';

import { Container, Content, InputGroup, Input, Button, Icon, View } from 'native-base';
import { replaceRoute } from '../../actions/route';
import { signInUserAsync } from '../../actions/users';

import theme from '../../themes/base-theme';
import styles from './styles';

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
          data: {
            email: '',
            password: ''
          },
          loading:false,
          isLoggedIn: false
        };
    }

    componentWillMount() {
      this.props.resetMe();
    }

    componentWillReceiveProps(nextProps) {
      if(nextProps.user.status === 'authenticated' && !nextProps.user.error) {
        this.props.replaceRoute('home');
      }

      //error
      //Throw error if it was not already thrown (check this.props.user.error to see if alert was already shown)
      //If u dont check this.props.user.error, u may throw error multiple times due to redux-form's validation errors
      if(nextProps.user.status === 'signin' && !nextProps.user.user && nextProps.user.error && !this.props.user.error) {
        alert(nextProps.user.error.message);
      }
    }

    onLogin(value) {
      this.props.login(value);
    }

    render() {
      var { signInUserAsync } = this.props;
        return (
            <Container theme={theme}>
                <View style={styles.container}>
                    <Content>
                        <Image source={require('../../../images/shadow.png')} style={styles.shadow}>
                            <View style={styles.bg}>
                                <InputGroup style={styles.input} key={'email'}>
                                    <Icon name="ios-person" />
                                    <Input placeholder="EMAIL"
                                        onChangeText={(text) => this.state.data.email = text} />
                                </InputGroup>
                                <InputGroup style={styles.input} key={'password'}>
                                    <Icon name="ios-unlock-outline" />
                                    <Input
                                        placeholder="PASSWORD"
                                        secureTextEntry={true}
                                        onChangeText={(text) => this.state.data.password = text}
                                    />
                                </InputGroup>
                                <Button style={styles.btn} textStyle={{color: '#fff'}} rounded
                                    onPress={() => this.onLogin(this.state.data)}>
                                    Login
                                </Button>
                            </View>
                        </Image>
                    </Content>
                </View>
            </Container>
        )
    }
}

function mapStateToProps(state, ownProps) {
  return {
    user: state.users
  };
}

function bindActions(dispatch){
  return {
      login:(value)=>dispatch(signInUserAsync(value, dispatch)),
      resetMe:() => {},
      replaceRoute:(route)=>dispatch(replaceRoute(route))
  }
}

export default connect(mapStateToProps, bindActions)(Login);
