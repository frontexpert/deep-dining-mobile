
'use strict';

import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Container, Header, Title, Content, View, Text, Button, Icon, H1, List, ListItem } from 'native-base';
import { Grid, Col, Row } from 'react-native-easy-grid';
import { openDrawer } from '../../actions/drawer';
import { replaceRoute } from '../../actions/route';
import { replaceOrPushRoute } from '../../actions/route';

import theme from '../../themes/base-theme';
import styles from './styles';

class Home extends Component {

    replaceRoute(route) {
        this.props.replaceRoute(route);
    }

    pushRoute(route) {
      this.props.replaceOrPushRoute(route);
    }

    render() {
        return (
            <Container theme={theme} style={{backgroundColor: '#515b66'}}>
                <Header style={{backgroundColor: '#54b59a'}} foregroundColor="#fff" >
                    <Button transparent onPress={() => this.replaceRoute('login')}>
                        <Icon name="ios-power" />
                    </Button>

                    <Title>Home</Title>

                    <Button transparent onPress={this.props.openDrawer}>
                        <Icon name="ios-menu" />
                    </Button>
                </Header>

                <Content>
                  <View style={styles.titleContainer}>
                    <H1 style={styles.textHeader}>
                        Welcome!
                    </H1>
                  </View>
                  <View padder>
                    <Text style={styles.textSubHeader}>
                        My Dinings:
                    </Text>
                    <List>
                        <ListItem iconLeft>
                            <Icon name="ios-people"/>
                            <Text>Daily Stand Up</Text>
                            <Text note>10:00 AM</Text>
                        </ListItem>
                        <ListItem iconLeft>
                            <Icon name="ios-people"/>
                            <Text>Finish list Screen</Text>
                            <Text note>By 2:00 PM</Text>
                        </ListItem>
                    </List>
                    <Button style={styles.btn} textStyle={{color: '#fff'}} rounded success onPress={() => this.pushRoute('inviteFriends')}>
                        ADD NEW
                    </Button>
                  </View>
                </Content>
            </Container>
        )
    }
}

function bindAction(dispatch) {
    return {
        openDrawer: ()=>dispatch(openDrawer()),
        replaceRoute:(route)=>dispatch(replaceRoute(route)),
        replaceOrPushRoute:(route)=>dispatch(replaceOrPushRoute(route))
    }
}

export default connect(null, bindAction)(Home);
