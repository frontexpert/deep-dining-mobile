'use strict';

var React = require('react-native');

var { StyleSheet, Dimensions } = React;

var deviceHeight = Dimensions.get('window').height;

module.exports = StyleSheet.create({
    links: {
      borderBottomWidth: 0
    },
    linkIcon: {
      fontSize: 22,
      lineHeight: 24,
      paddingLeft: 9,
      paddingRight: 6,
      alignSelf: 'center',
      borderWidth: 1,
      borderColor: '#fff',
      borderRadius: 14,
      marginRight: 8
    },
    input: {
      marginBottom: 20
    },
    modal: {

    },
    addPersonModal: {
      height: deviceHeight,
      backgroundColor: "#25303e"
    },
    modalSpace: {
      paddingTop: 20,
      paddingLeft: 10,
      paddingRight: 10,
      paddingBottom: 30,
    },
    btnModalClose: {
      alignSelf: 'flex-end'
    }
});
