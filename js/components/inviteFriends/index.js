
'use strict';

import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Container, Header, Title, Content, Text, Button, List, View, ListItem, Icon, Thumbnail, InputGroup, Input } from 'native-base';
import { openDrawer } from '../../actions/drawer';
import { popRoute } from '../../actions/route';

import Modal from 'react-native-modalbox';
// import { Modal, TouchableHighlight } from 'react-native';
import theme from '../../themes/base-theme';
import styles from './styles';

class InviteFriends extends Component {
    constructor(props) {
        super(props);
        this.state = {
            swipeToClose: true
        }
    }
    popRoute() {
        this.props.popRoute();
    }

    setModalVisible(visible) {
      this.setState({modalVisible: visible});
    }
    
    openModal() {
        this.refs.addPerson.open()
    }
    closeModal() {
        this.refs.addPerson.close()
    }

    render() {
        return (
            <Container theme={theme} style={{backgroundColor: '#515b66'}}>
                <Header style={{backgroundColor: '#54b59a'}} foregroundColor="#fff">
                    <Button transparent onPress={() => this.popRoute()}>
                        <Icon name="ios-arrow-back" />
                    </Button>

                    <Title>Invite Friends</Title>

                    <Button transparent onPress={this.props.openDrawer}>
                        <Icon name="ios-menu" />
                    </Button>
                </Header>

                <Content padder>
                  <View>
                    <List>
                        <ListItem>
                          <Thumbnail source={require('../../../images/sanket.png')} />
                          <Text>You</Text>
                        </ListItem>
                        <ListItem button iconLeft style={styles.links} onPress={this.openModal.bind(this)}>
                            <Icon style={styles.linkIcon} name="ios-add" />
                            <Text>Add</Text>
                        </ListItem>
                    </List>
                  </View>
                  <Modal
                    style={[styles.modal, styles.addPersonModal]}
                    backdrop={false}
                    ref={"addPerson"}
                    swipeToClose={this.state.swipeToClose} >
                      <Button style={styles.btnModalClose} transparent onPress={this.closeModal.bind(this)} >
                        <Icon name="ios-close" />
                      </Button>
                      <View style={styles.modalSpace}>
                        <InputGroup style={styles.input}>
                            <Icon name="ios-person-add" />
                            <Input placeholder="Enter Friend" />
                        </InputGroup>
                      </View>
                   </Modal>
                </Content>
            </Container>
        )
    }
}

function bindAction(dispatch) {
    return {
        openDrawer: ()=>dispatch(openDrawer()),
        popRoute: () => dispatch(popRoute())
    }
}

export default connect(null, bindAction)(InviteFriends);
